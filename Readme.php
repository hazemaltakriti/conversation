

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title> Bootstrap Tables | Clear Admin Template </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" rel="stylesheet" href="css/app.css"/>
    <!-- end of global css -->
    <!--page level css -->
    <link rel="stylesheet" href="vendors/bootstrap-table/css/bootstrap-table.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" href="css/custom_css/skins/skin-default.css" type="text/css" id="skin"/>
    <link rel="stylesheet" type="text/css" href="css/custom_css/bootstrap_tables.css">
    <!--end of page level css-->
</head>

<body class="skin-default">
<div class="panel filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="ti-list"></i> Sortable Bootstrap Table
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="bootstrap-table"><div class="fixed-table-toolbar"><div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div></div><div class="fixed-table-container" style="height: 290px; padding-bottom: 40px;"><div class="fixed-table-header" style="margin-right: 0px;"><table class="table table-hover" style="width: 998px;"><thead><tr><th style="" data-field="firstname" tabindex="0"><div class="th-inner sortable both">First name</div><div class="fht-cell" style="width: 116px;"></div></th><th style="" data-field="lastname" tabindex="0"><div class="th-inner sortable both">Last name</div><div class="fht-cell" style="width: 114px;"></div></th><th style="" data-field="position" tabindex="0"><div class="th-inner sortable both">Position</div><div class="fht-cell" style="width: 254px;"></div></th><th style="" data-field="office" tabindex="0"><div class="th-inner sortable both">Office</div><div class="fht-cell" style="width: 155px;"></div></th><th style="" data-field="startdate" tabindex="0"><div class="th-inner sortable both">Start date</div><div class="fht-cell" style="width: 112px;"></div></th><th style="" data-field="mail" tabindex="0"><div class="th-inner sortable both">E-mail</div><div class="fht-cell" style="width: 242px;"></div></th></tr></thead></table></div><div class="fixed-table-body"><div class="fixed-table-loading" style="top: 41px;">Loading, please wait...</div><table data-toggle="table" data-sort-name="age" data-sort-order="desc" data-pagination="true" data-search="true" data-height="400" class="table table-hover" style="margin-top: -40px;">
                                <thead><tr><th style="" data-field="firstname" tabindex="0"><div class="th-inner sortable both">First name</div><div class="fht-cell"></div></th><th style="" data-field="lastname" tabindex="0"><div class="th-inner sortable both">Last name</div><div class="fht-cell"></div></th><th style="" data-field="position" tabindex="0"><div class="th-inner sortable both">Position</div><div class="fht-cell"></div></th><th style="" data-field="office" tabindex="0"><div class="th-inner sortable both">Office</div><div class="fht-cell"></div></th><th style="" data-field="startdate" tabindex="0"><div class="th-inner sortable both">Start date</div><div class="fht-cell"></div></th><th style="" data-field="mail" tabindex="0"><div class="th-inner sortable both">E-mail</div><div class="fht-cell"></div></th></tr></thead>
                                <tbody><tr data-index="0"><td style="">Estrella</td><td style="">Weimann</td><td style="">Lead Identity Technician</td><td style="">Port Kamille</td><td style="">1988-3-6</td><td style="">Estrella.Weimann@hotmail.com</td></tr><tr data-index="1"><td style="">Brock</td><td style="">Ward</td><td style="">International Assurance Director</td><td style="">Abernathyton</td><td style="">1986-12-24</td><td style="">Brock.Ward45@hotmail.com</td></tr><tr data-index="2"><td style="">Cortney</td><td style="">Hessel</td><td style="">Lead Metrics Coordinator</td><td style="">West Kareemstad</td><td style="">1946-12-28</td><td style="">Cortney_Hessel92@hotmail.com</td></tr><tr data-index="3"><td style="">Rebeca</td><td style="">Ortiz</td><td style="">Internal Markets Engineer</td><td style="">Gorczanyhaven</td><td style="">1989-6-21</td><td style="">Rebeca.Ortiz93@gmail.com</td></tr><tr data-index="4"><td style="">Rachelle</td><td style="">Ward</td><td style="">Legacy Brand Director</td><td style="">Connellymouth</td><td style="">1950-3-29</td><td style="">Rachelle28@yahoo.com</td></tr><tr data-index="5"><td style="">Candelario</td><td style="">Hilpert</td><td style="">Future Data Supervisor</td><td style="">North Herminashire</td><td style="">1977-8-22</td><td style="">Candelario.Hilpert0@gmail.com</td></tr><tr data-index="6"><td style="">Iva</td><td style="">Fritsch</td><td style="">Dynamic Communications Planner</td><td style="">Deionville</td><td style="">1948-3-18</td><td style="">Iva.Fritsch@yahoo.com</td></tr><tr data-index="7"><td style="">Estella</td><td style="">Gerhold</td><td style="">National Optimization Producer</td><td style="">Deckowberg</td><td style="">1963-1-17</td><td style="">Estella_Gerhold73@hotmail.com</td></tr><tr data-index="8"><td style="">Emiliano</td><td style="">Huel</td><td style="">Global Intranet Analyst</td><td style="">North Creolaburgh</td><td style="">1946-4-29</td><td style="">Emiliano.Huel@gmail.com</td></tr><tr data-index="9"><td style="">Erika</td><td style="">Reichert</td><td style="">Corporate Optimization Strategist</td><td style="">East Andreane</td><td style="">1952-9-15</td><td style="">Erika36@yahoo.com</td></tr></tbody>
                            </table></div><div class="fixed-table-footer" style="display: none;"><table><tbody><tr></tr></tbody></table></div><div class="fixed-table-pagination"><div class="pull-left pagination-detail"><span class="pagination-info">Showing 1 to 10 of 40 rows</span><span class="page-list"><span class="btn-group dropup"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="page-size">10</span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li class="active"><a href="javascript:void(0)">10</a></li><li><a href="javascript:void(0)">25</a></li><li><a href="javascript:void(0)">50</a></li></ul></span> rows per page</span></div><div class="pull-right pagination"><ul class="pagination"><li class="page-pre"><a href="javascript:void(0)">‹</a></li><li class="page-number active"><a href="javascript:void(0)">1</a></li><li class="page-number"><a href="javascript:void(0)">2</a></li><li class="page-number"><a href="javascript:void(0)">3</a></li><li class="page-number"><a href="javascript:void(0)">4</a></li><li class="page-next"><a href="javascript:void(0)">›</a></li></ul></div></div></div></div><div class="clearfix"></div>
                        </div>
</div>
<!-- wrapper-->
<!-- global js -->
<script src="js/app.js" type="text/javascript"></script>
<!-- end of global js -->
<!-- begining of page level js -->
<script type="text/javascript" src="vendors/editable-table/js/mindmup-editabletable.js"></script>
<script type="text/javascript" src="vendors/bootstrap-table/js/bootstrap-table.min.js"></script>
<script type="text/javascript" src="vendors/tableExport.jquery.plugin/tableExport.min.js"></script>
<script src="js/custom_js/bootstrap_tables.js" type="text/javascript"></script>
<!-- end of page level js -->
</body>

</html>
