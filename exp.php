    create database TelephoneBook;
        --table users 
    create table tbl_users    
                    (
                    id  int NOT NULL AUTO_INCREMENT  ,
                    fullname varchar(50) not null,
                    Email varchar(100),
                    password varchar(100),
                    address varchar(100),    
                    ip varchar(50),
                    username varchar(50) not null,
                    country ENUM varchar(50),
                    activation int(6) not null,
                    validate boolean DEFAULT 0,
                    forgetpassword varchar(50)  NOT Null,
                    primary key(id),
                    UNIQUE (Email),
                    UNIQUE(username)
                    );
                    CREATE INDEX idx ON tbl_users (id, name, Email,password);
        --table for contacts which users will be fill
        --aliasesusers
    create table tbl_aliasesusers 
                            (
                            id  int NOT NULL AUTO_INCREMENT,
                            id_u int NOT NULL,
                            username varchar(50) ,
                            password varchar(50),
                            access ENUM('read','write','read and write') NOT NULL
                            ,primary key(id),
                            foreign key(id_u) references tbl_users(id) ON DELETE CASCADE,
                            UNIQUE (username)
                            );
                    CREATE INDEX idx ON tbl_aliasesusers (id, username);
        
            --table for save contacts
    create table tbl_contacts
                        (
                        id  int NOT NULL AUTO_INCREMENT ,
                        id_u int(6) not null,
                        name varchar(50),
                        Email varchar(100),
                        date varchar(100),
                        number varchar(50),
                        foreign key(id_u) references tbl_users(id) ON DELETE CASCADE,
                        primary key(id)
                        );
                        CREATE INDEX idx ON tbl_contacts (id_u);
        --this table for recording all login date of users;
    create table tbl_dateLogin
                        (
                            id int Not Null AUTO_INCREMENT,
                            id_u int(6) not null,
                            datereg varchar(50) not null,
                            foreign key(id_u) references tbl_users(id) ON DELETE CASCADE,
                            primary key(id)
                        );
                    CREATE INDEX idx ON tbl_dateLogin (id_u);
